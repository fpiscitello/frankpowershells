#$WCUID="0000090"
#$PreferredGivenName = "Sam"

function UpdatePreferredName 
{
    param(
        [Parameter(mandatory=$true)] [string] $WCUID, 
        [Parameter(mandatory=$true)] [string] $PreferredGivenName
        )
    $users=get-aduser -filter {EmployeeID -eq $WCUID} -Properties *
    $users.count
    Foreach ($user in $users)
    {
        write-host ($WCUID,":Preferred First Name: Adding to",$user.name,"the name",$PreferredGivenName)
        $newDisplayName = $user.surname + ", " + $preferredGivenName
        if ($user.mail -ne $null)
        {
        
            if ($user.department -eq "Student")
            {
                $newProxyAddress = "smtp:" + $PreferredGivenName.Substring(0,1) + $user.surname.Substring(0,1) + $user.EmployeeID.Substring(1,6)+"@wcupa.edu"
            }
            else 
            {
                $newProxyAddress = "smtp:" + $PreferredGivenName.Substring(0,1) + $user.surname+"@wcupa.edu"
            }
            $user|set-aduser -add @{proxyAddresses = $newProxyAddress }
        }
        $user|set-aduser -displayname $newDisplayName -replace @{passhePreferredGivenName = $PreferredGivenName}
    }
}
