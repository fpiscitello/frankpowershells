#Powershell Hints and snips

#Standard Shell for VSCode "terminal.integrated.shell.windows": "C:\\WINDOWS\\System32\\WindowsPowerShell\\v1.0\\powershell.exe",

#add Admin Modules
  Import-Module ActiveDirectory
  Import-Module ExchangeOnlineManagement

#build WCU People
  Import-Module "C:\PowerShellScripts\Modules\ADAccountCreationModule.psm1" -Force
  new-account

#get old Office365 connection tool
  . "C:\Program Files\WindowsPowerShell\Scripts\Connect-Office365.ps1"

#Connect to ExOL V2
  Connect-ExchangeOnline -userprincipalname frankpo365@wcupa.onmicrosoft.com
  
#for O365 compliance module stuff
  Connect-IPPSSession -userprincipalname frankpo365@wcupa.onmicrosoft.com

#add Exchange 2013
$ExSession = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri http://wcuxchp01.passhe.lcl/PowerShell/ -Authentication Kerberos
Import-PSSession $ExSession

#Disable Mailbox:
#Hides from GAL, Removes RUS requirement, Sets email address to fake one, removes all SMTP aliases.

  $UserID="75aa778087"
  $alias=get-mailbox $userID|Select-Object alias
  $alias=$alias.alias
  get-user $UserID |set-mailbox -emailaddresspolicyenabled $false -RequireSenderAuthenticationEnabled $true -HiddenFromAddressListsEnabled $true -emailaddresses "$alias-blk@wcupa.edu"
  get-mailbox $UserID |Set-CASMailbox -ewsenabled $false -popenabled $false -imapenabled $false -activesyncenabled $false -mapienabled $false -owaenabled $false

#Enable Mailbox
#Reveals to GAL, Adds RUS Enforcement to restore default SMTP addresses

  $UserID="75testuser"
  get-user $UserID |set-mailbox -emailaddresspolicyenabled $true -RequireSenderAuthenticationEnabled $false -HiddenFromAddressListsEnabled $false 
  get-mailbox $UserID |Set-CASMailbox -ewsenabled $true -popenabled $true -imapenabled $true -activesyncenabled $true -mapienabled $true -owaenabled $true

#Gets disabled user accounts in OU for West Chester accounts

  get-user -Recipienttype "UserMailbox" -organizationalUnit "Employees - Disabled" -filter "Company -like 'West Chester*'" -resultsize unlimited |Where-Object {$_.userAccountControl -match "AccountDisabled"} 

#add full mailbox permissions to generic account

  Add-MailboxPermission {generic}@wcupa.edu -user {user}@wcupa.edu -AccessRights fullaccess
  get-user 75{generic}|add-adpermission -user {user}@wcupa.edu -extendedRights "send as"

#get all students in OU without mailboxes enabled

  $theList=get-aduser -SearchBase "ou=students,ou=wcu,ou=campuses,dc=passhe,dc=lcl" -filter {mail -notlike "*"}

#enable mailboxes for all students in $theList

  foreach ($user in $theList){enable-mailbox $user.SAMAccountName -alias $user.name -database 'WCU-DAG1-DB24'}
  foreach ($user in $theList){set-mailbox $user.SAMAccountName -customattribute14 "WCU" -customattribute15 "0" -recipientLimits 100}

#Get expiring Users in 30 days

  Get-ADUser -SearchBase $OU -Properties AccountExpirationDate -filter * | Where-Object {$_.AccountExpirationDate -ge (Get-Date) -and $_.AccountExpirationDate -le (Get-Date).AddDays(30)} | Sort-Object AccountExpirationDate | Format-Table AccountExpirationDate,GivenName,Surname,SamAccountName,UserPrincipalName,Enabled -AutoSize
  set-mailbox {$user} -CustomAttribute13 "Separtated"
  Get-ADUser -SearchBase "ou=Keepers,ou=Students-ToBeDeleted,ou=WCU,ou=campuses,dc=passhe,dc=lcl" -properties displayName -filter *|ForEach-Object {Connect-Mailbox -Identity $_.DisplayName -database wcu-dag1-db5}

#Find Proxy Addressess

  $findAddress="research@wcupa.edu"
  get-aduser -LDAPFilter "(&(proxyAddresses=*)(proxyAddresses=smtp:$findAddress))" -properties proxyAddresses

#Get Litigation Hold Mailboxes

  Get-Mailbox -Filter {LitigationHOldEnabled -eq $true} | Select-Object name,displayname,litigationHoldDate,litigationHoldOwner,RetentionComment | Format-Table
  
  Get-Mailbox -Filter {LitigationHOldEnabled -eq $true -and RetentionComment -like "*RT-1221*"} | Select-Object name,displayname,litigationHoldDate,RetentionComment |Sort-Object displayname |Format-Table

#Extract Security Logs from PC
	psexec \\computer-w7 wevtutil epl security c:\security.evtx

#rejoin disconnected mailboxes
Get-MailboxDatabase | Get-MailboxStatistics | Where-Object { $_.DisplayName -eq "Walker, Roland F" } | Format-List DisplayName,Database,DisconnectReason 

#Distribution Group Owner
Set-DistributionGroup "SecurityGroupX" -ManagedBy "stephen@example.com" -BypassSecurityGroupManagerCheck
Get-DistributionGroup "SecurityGroupX" | Select-Object ManagedBy -ExpandProperty "ManagedBy"

#Distribution List Manage Users
$theList = get-mailbox -filter 'customattribute10 -like "*06*"'
$theList | ForEach-Object {Add-DistributionGroupMember -Identity "wcu-allmanagers" -Member $_.userprincipalname}

get-aduser -filter "Department -eq 'Student'" |ForEach-Object {Add-ADGroupMember "WCU-StudentsWithPasswordPolicy" -Member $_}

#Get users that have corrupted mailbox attributes.
$listofUsers=get-aduser -properties * -ldapfilter "((mailnickname=*)(!mail=*))" -SearchBase "ou=students,ou=wcu,ou=campuses,dc=passhe,dc=lcl"
$listOfUsers |ForEach-Object {Enable-Mailbox -Identity $_.SamAccountName -alias $_.cn -PrimarySmtpAddress $_.UserPrincipalName}

#Update Address Books
#get-AddressList | update-AddressList
get-AddressList -searchtext "West Chester"| update-AddressList
Get-GlobalAddressList | Update-GlobalAddressList
Get-OfflineAddressBook | Update-OfflineAddressBook

#Get Dynamic Group Members
$FTE = Get-DynamicDistributionGroup "Full Time Employees"
Get-Recipient -RecipientPreviewFilter $FTE.RecipientFilter

#get all employees into a CSV
Get-ADGroupMember "WCU-All Employees" | ForEach-Object {get-aduser -identity $_.SamAccountName -properties department,displayname,samaccountname,physicaldeliveryofficename} |Select-Object samaccountname,displayname,department,physicaldeliveryOfficeName |export-csv "WCU-AllEmployees.txt" -Delimiter ";"

#forward Separated Employees
$user = "75gpillay"
$forwardTo = "gpillay@hushmail.com"
set-mailbox $user -ForwardingSmtpAddress $forwardTo -DeliverToMailboxAndForward $false -CustomAttribute13 "Forwarded"
Set-MailboxAutoReplyConfiguration $user -InternalMessage "This mailbox is no longer active at West Chester University of PA." -ExternalMessage "This mailbox is no longer active at West Chester University of PA." -ExternalAudience all -AutoReplyState enabled

#get NDR report settings
Get-DistributionGroup "wcu-all active students" |Format-List *report*
	#ReportToManagerEnabled    : False
	#ReportToOriginatorEnabled : True

#get mailbox fullaccess users
Get-MailboxPermission 75president | Where-Object { ($_.IsInherited -eq $false) -and ($_.user -like "passhe\75*")} |Select-Object user, accessrights

#enable student mailbox with existing AD object
$username="da123456"
Enable-Mailbox $username@wcupa.edu -alias $username -Database WCUEXCHDAGP01-PROVISIONING -primarySmtpAddress $username@wcupa.edu -DomainController wcuaddcp03.passhe.lcl
Add-MailboxPermission $username@wcupa.edu -User 'PASSHE\WCU-Mailbox Admins' -AccessRights 'FullAccess' -DomainController wcuaddcp03.passhe.lcl
Set-Mailbox $username@wcupa.edu -CustomAttribute14 "WCU" -CustomAttribute15 "0" -RetentionPolicy 'WCU - Default Archive Policy' -EmailAddressPolicyEnabled $true -RecipientLimits 100 -addressbookpolicy 'WCU-Student Address Book Policy' -DomainController wcuaddcp03.passhe.lcl

$theList=Get-GPO -all |Where-Object {$_.displayname -like "Citrix*"}|Select-Object id,displayname
$theList|ForEach-Object {set-gppermission -guid $_.id -permissionlevel GpoEditDeleteModifySecurity -targetName "WCUCitrixAdmins" -targetType group}

#test computers in OU
Get-adcomputer -properties name, passwordlastset -filter {enabled -eq $true} -SearchBase "ou=buildings,dc=wcupa,dc=net" -SearchScope onelevel -Server wcudcp04.wcupa.net | foreach-object {Test-Connection $_.dnshostname -count 1}

#get authorized DL senders
#O365 version
$users=Get-DistributionGroup "WCU-Student Workers Missing Clearances" |Select-Object -expandproperty acceptmessagesonlyfrom
foreach ($person in $users) {get-aduser -properties displayname -filter {cn -eq $person}|Select-Object displayname}

#get authorized DL senders
#AD Only Version
$users=Get-adgroup "WCU-Enrolled Students" -Properties authorig |Select-Object -expandproperty authorig
foreach ($person in $users) {get-aduser -properties displayname $person |Select-Object displayname}

#set authorized DL Senders (onPrem)
set-adgroup -Identity "WCU-Dept-Distance Education - Faculty" -server wcuaddcp01.passhe.lcl -add @{authOrig=@("CN=RLI,OU=Employees,OU=WCU,OU=Campuses,DC=PASSHE,DC=LCL")}

#Email Enable General Team as a DL too.
set-unifiedGroup "Deans" -subscriptionEnabled:$true
set-unifiedGroup "Deans" -AutoSubscribeNewMembers:$true
set-mailbox -identity "Deans3039@WCUPA.onmicrosoft.com" -groupMailbox -forwardingSMTPAddress "90a7dc04.WCUPA.onmicrosoft.com@amer.teams.ms"



#litigation Hold Stuff

get-compliancecase -CaseType AdvancedEdiscovery
get-caseholdpolicy -case "LitHold - REQ0017249" -distributiondetail |Select-Object -ExpandProperty exchangelocation |Format-Table displayname,name
get-mailbox frank@wcupa.edu | Format-List inPlaceHolds,RetentionComment

get-caseholdpolicy 6471f916-b0e0-4ddb-9857-51450c5c9745 |Select-Object -ExpandProperty exchangelocation |Format-Table displayname,name
get-caseholdpolicy 6471f916-b0e0-4ddb-9857-51450c5c9745 |Select-Object -ExpandProperty sharepointlocation |Format-Table displayname,name

get-caseholdpolicy -case "LitHold - REQ0017249" -DistributionDetail |Select-Object -ExpandProperty exchangeLocation |Format-Table displayname,name
get-caseholdpolicy -case "LitHold - REQ0017249" -DistributionDetail |Select-Object -ExpandProperty SharepointLocation |Format-Table displayname,name

get-caseholdpolicy 458f47d3-8a67-486e-9ad3-d51cb69ea7ad |Format-List caseID
Get-ComplianceCase e83d7de1-8664-4427-b6cb-7cc205fd6f17

#newTools
# run this if the script is not installed anywhere "install-script -name get-userholdpolicies"

Get-Mailbox josgood@wcupa.edu | & 'c:\powershellscripts\office365\Get-UserHoldPolicies.ps1' |ft username,"hold placed by","case name"

#compare 2 AD Groups
$theList2=Compare-Object -ReferenceObject (Get-AdGroupMember "WCU-Dept-InfoSec-Duo-ShibUsers" | select name | sort-object -Property name) -DifferenceObject (Get-AdGroupMember "WCU-All Employees" | select "name" | sort-object -Property name) -property name -passthru

#set mailbox permissions o365
$mbname="em905477@wcupa.edu"
$userName="prssa@wcupa.edu"
Add-MailboxPermission $mbname -user $userName -AccessRights FullAccess -AutoMapping $true
Add-recipientPermission $mbname  -trustee $UserName -AccessRights SendAs -Confirm:$false
 #testing

#remove mailbox permissions o365
$mbname="finaid@wcupa.edu"
$userName="bfenton@wcupa.edu"
remove-MailboxPermission $mbname -user $userName -AccessRights FullAccess -Confirm:$false
remove-recipientPermission $mbname  -trustee $UserName -AccessRights SendAs -Confirm:$false
#testing
